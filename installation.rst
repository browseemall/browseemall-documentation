.. _`Installation`:

Installation
============

BrowseEmAll is a native application which can be used on Windows, macOS and Linux. To start using the application you will need to download and install it.

System Requirements
-------------------
Your system should meet the following criteria if you want to run BrowseEmAll:

**Operating Systems**
* Windows 7, 8, 8.1 or 10 (32 or 64bit)
* Windows Server 2012 R2 or newer
* macOs / OS X 10.9 or higher
* Linux (most distros)

**RAM**
You will need at least *4 GB* to fully use the application. We recommend *8 GB* or more for best performance.

**Disk Space**
Depending on the number of browsers you want to use at least *10 GB* of free disk space are needed. To use all browsers provided by BrowseEmAll you need around *50 GB* of free disk space.

.. warning::
    
    For some browsers the application will spin up a Virtual Machine (like Internet Explorer on macOS) so make sure to activate virtualization support if you want to run BrowseEmAll inside a VM. 


Download BrowseEmAll
--------------------

You can download the appropriate setup for your operating system in the `Downloads <https://www.browseemall.com/Try>`_ section.

Installation
-------------------

The installation depends on your platform:

**Windows**

You can just execute the downloaded BrowseEmAll.exe which will launch a standard windows setup.

**macOS / OS X**

Mount the downloaded .dmg file and copy BrowseEmAll.Mac to your /Applications folder. 

**Linux**

Install the downloaded setup file. The details depend on your distribution and configuration. 

Activate Licence or Trial
-------------------------

On the first run of the application you will be asked for your licence key. If you don't already have one you can just enter your email address to start the free trial. 

.. image:: _static/licence.png